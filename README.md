Dybbuk - list manager 
======
Dybbuk is a list managing app designed to expand on existing list creators.

Why did we create Dybbuk?
------
We found that other popular offerings such as MAL didn't completely satisfy our needs, and so decided to create our own extension to it.\
And if we're already at it, we plan to make it compatible with more APIs.

How is Dybbuk different from other offerings such as MAL?
------
Dybbuk isn't a database. We don't hold a catalog of all anime, manga, games, etc that have ever existed.\
What we do is take a list you created in another site, and using their API, shows you a reorganized list.\
\
Essentially, we take your list from another source, for example MAL, and add a layer of ordering.\
Instead of simply using a simply 1 to 10 scoring system, we add a comparative order in each score/number. In other words, a hierarchy.\
\
This way, we can see that we prefer Jojo part 2 over Jojo part 1, even though they are both scores are 9.

Examples of API/DB we are considering:
------
- MyAnimeList.net (for anime and manga)
- imdb.com (for movies and tv shows)
- *insert_games_DB_API* (for video games)
- *insert_books_DB_API* (for books)


